


## To create a SASL/SCRAM user
    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name connectAdmin --alter --add-config 'SCRAM-SHA-512=[password=connectAdmin]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name restUser --alter --add-config 'SCRAM-SHA-512=[password=restUser]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name ksqlDBAdmin --alter --add-config 'SCRAM-SHA-512=[password=ksqlDBAdmin]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name uiClient --alter --add-config 'SCRAM-SHA-512=[password=uiClient]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name client --alter --add-config 'SCRAM-SHA-512=[password=client-secret]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name JohnDoe --alter --add-config 'SCRAM-SHA-512=[password=TIdoe123]'


## To check all the brokers connected to the Zookeeper running in 2-way SSL mode
    zookeeper-shell.sh localhost:2183 -zk-tls-config-file sasl-scram/zookeeper-client.properties ls /brokers/ids

## Test schema registry
    curl localhost:8081/subjects and get [] as a response.
    curl -u "karapaceRest:karapace-rest-secret" localhost:8085/topics

## To delete a SASL/SCRAM user
    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name karapaceRest --alter --delete-config 'SCRAM-SHA-512'


## Example ACL commands
### To grant PRODUCER access for the user to the topic mytopic
    kafka-acls.sh --authorizer-properties zookeeper.connect=localhost:2182 --zk-tls-config-file zookeeper-client.properties --add --allow-principal User:my-user --operation WRITE --operation DESCRIBE --operation DESCRIBECONFIGS --topic mytopic

### To grant CONSUMER access for the user to the topic mytopic and with consumer group mycgroup
    kafka-acls.sh --authorizer-properties zookeeper.connect=localhost:2182 --zk-tls-config-file zookeeper-client.properties --add --allow-principal User:my-user --operation READ --operation DESCRIBE --topic mytopic

    kafka-acls.sh --authorizer-properties zookeeper.connect=localhost:2182 --zk-tls-config-file zookeeper-client.properties --add --allow-principal User:my-user --operation READ --group mycgroup

## console clients
    kafka-console-producer.sh --bootstrap-server DOCKER_HOST_IP:9093,DOCKER_HOST_IP:9094 --topic test-topic --producer.config client_security.properties

    kafka-console-consumer.sh --bootstrap-server SASL_SSL://DOCKER_HOST_IP:9093,DOCKER_HOST_IP:9094 --topic test-topic --consumer.config client_security.properties


## Get certificate
keytool -keystore kafka.truststore.jks -export -alias CARoot -rfc