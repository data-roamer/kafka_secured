#  Kafka
Set up a two-way TLS/SSL for zookeeper and its clients, two-way SSL for inter-broker connection and  SASL_SSL (SCRAM) for kafka broker and its clients. 

## Step 1
Run kafka-generate-ssl.sh file and follow the steps to create CA, Keystore and Truststore. You should have generated:
1. A truststore file
2. Private key for the CA (ca-key)
3. A keystore file for every broker i.e if setting up 2 brokers then 2 keystores
4. Zookeeper server keystore
5. Zookeeper client keystore (because of two-way TLS/SSL, the client will authenticate using SSL) 


## Step 2
Edit the zookeeper-client.properties file and Create a SASL/SCRAM users. Make sure to change the passwords/usernames

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name connectAdmin --alter --add-config 'SCRAM-SHA-512=[password=connectAdmin]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name restUser --alter --add-config 'SCRAM-SHA-512=[password=restUser]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name ksqlDBAdmin --alter --add-config 'SCRAM-SHA-512=[password=ksqlDBAdmin]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name uiClient --alter --add-config 'SCRAM-SHA-512=[password=uiClient]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name client --alter --add-config 'SCRAM-SHA-512=[password=client-secret]'

    kafka-configs.sh --zookeeper localhost:2183 --zk-tls-config-file sasl-scram/zookeeper-client.properties --entity-type users --entity-name JohnDoe --alter --add-config 'SCRAM-SHA-512=[password=TIdoe123]'

## Step 3
Create .env file from .env-example and start the cluster
    docker-compose up -d