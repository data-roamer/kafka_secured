#!/usr/bin/env bash

set -e

DEFAULT_TRUSTSTORE_FILENAME="kafka.truststore.jks"
TRUSTSTORE_WORKING_DIRECTORY="truststore"

echo
echo "Welcome to the Kafka SSL truststore generator script."

mkdir $TRUSTSTORE_WORKING_DIRECTORY
echo
echo -n "Enter the path of the trust store file. [ca-cert]: "
read -e CA_CERT_FILE

echo
echo "Now the trust store will be generated from the certificate."
echo
echo "You will be prompted for:"
echo " - the trust store's password (labeled 'keystore'). Remember this"
echo " - a confirmation that you want to import the certificate"

keytool -keystore $TRUSTSTORE_WORKING_DIRECTORY/$DEFAULT_TRUSTSTORE_FILENAME \
-alias CARoot -import -file $CA_CERT_FILE

trust_store_file="$TRUSTSTORE_WORKING_DIRECTORY/$DEFAULT_TRUSTSTORE_FILENAME"

echo
echo "$TRUSTSTORE_WORKING_DIRECTORY/$DEFAULT_TRUSTSTORE_FILENAME was created."

echo "   Once this certificate has been stored in the trust"
echo "   store, it will be deleted. It can be retrieved from the trust store via:"
echo "   $ keytool -keystore <trust-store-file> -export -alias CARoot -rfc"

rm $CA_CERT_FILE

echo
echo "All done!"

